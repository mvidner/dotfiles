# sourced by Ubuntu ~/.bashrc                                    -*- sh -*-

# define an alias and also enable its shell completion
alias_c() {
    alias $1=$2
    if   complete -p $2 &>/dev/null; then
        `complete -p $2` $1
    fi
}

alias bx="bundle exec"
alias df="df -x rootfs -x devtmpfs -x tmpfs -h"
e() {
    local -a args
    local f line
    for f in "$@"; do
        if  [[ "$f" =~ : ]]; then
            # grep or compiler output: file:line:...
            line="${f#*:}"; line="${line%%:*}"
            args=(${args[@]} ${line:++${line}} ${f%%:*})
        elif [[ "$f" =~ ^[ab]/ && ! -f "$f" ]]; then
            # git diff: a/file b/file
            args=(${args[@]} "${f#?/}")
        else
            args=(${args[@]} "$f")
        fi
    done
    emacsclient -n "${args[@]}"
}
alias_c g  git
alias_c gi git
# commit with date taken from the files in the index
git-commit-date-index() {
    local last_changed_file=$(git status -s | sed -n 's/^[AM]..\(.*\)/\1/;T;p' | xargs ls -t | head -n1)
    git commit --date="$(date -R -r $last_changed_file)"
}
gk() { git status & gitk &>/dev/null --all "$@" & }
alias ip="ip --color --brief"
alias_c jct journalctl
alias ltr="ls -ltr | tail"
mcd() { mkdir "$1" && cd "$1"; }
alias o=less
# psmisc.rpm
alias pt="pstree -pluna |less"
alias rgrep="grep -rn \
 --exclude-dir=.git --exclude-dir=build --exclude-dir=autodocs \
 --exclude-dir=.deps --exclude-dir=.libs \
 --exclude='*~' --exclude=jquery.js"
# ruby-require-gem-eval
#  because ruby 1.8 -r does not work for gems
rubyrge() {
  local  GEM="${1?\$1 is GEM name}"
  local EXPR="${2?\$2 is EXPR to evaluate}"
  ruby -r rubygems -e "require '$GEM'; $EXPR"
}
alias_c sct systemctl
alias ssa="ssh-add -l | grep ssh/id || ssh-add"
alias_c z zypper

unset -f alias_c
