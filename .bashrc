# From mvidner/dotfiles

# aliases
if [ -s ~/.alias ]; then
      . ~/.alias
fi

# SUSE command-not-found.rpm
export COMMAND_NOT_FOUND_AUTO=1

# http://github.com/guides/put-your-git-branch-name-in-your-shell-prompt
if [ "$(type -t __git_ps1)" ]; then
  PS1="\$(__git_ps1 '(%s) ')$PS1"
fi

# better bash debugging
PS4='+\h:${BASH_SOURCE}@${LINENO}(${FUNCNAME[0]}): '

PATH=$PATH:~/.gem/ruby/2.1.0/bin
