;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

; simplified from SUSE /etc/skel/.emacs
(load "~/.gnu-emacs" nil t)

(setq custom-file "~/.gnu-emacs-custom")
(load "~/.gnu-emacs-custom" t t)
(put 'narrow-to-region 'disabled nil)
