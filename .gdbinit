# https://sourceware.org/gdb/wiki/STLSupport
# It looks like it should be done automatically by
#   /usr/share/gdb/auto-load/usr/lib64/libstdc++.so.6.0.21-gdb.py
# but somehow it does not work out
python
import sys
sys.path.insert(0, '/usr/share/gcc-5/python')
from libstdcxx.v6.printers import register_libstdcxx_printers
register_libstdcxx_printers(None)
end
# http://www.root.cz/clanek/1867 aka https://www.root.cz/clanky/gdb-pro-normalni-lidi/
set history filename /home/mvidner/.gdb_history
set history save on
