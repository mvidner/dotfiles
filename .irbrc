require 'irb/completion'
# http://blog.nicksieger.com/articles/2006/04/23/tweaking-irb
require 'irb/ext/save-history'
IRB.conf[:SAVE_HISTORY] = 100
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb_history"
